import { Request, Response } from 'express';
import exampleModel from '../models/exampleModel';

export const exampleIndex = () => {
  return exampleModel.get();
};

export default { exampleIndex };
