import express from 'express';
import exampleRoutes from './routes/exampleRoutes';

const app: express.Application = express();
const port = 7000;

app.use('/', exampleRoutes);

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
