const testData = {
  example: 'this is working',
  date: ['example', 'example', 'example']
};

const get = () => {
  return testData;
};

export default { get };
