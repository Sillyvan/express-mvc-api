import express from 'express';
import exampleController from '../controllers/exampleController';

const router = express.Router();

router.get('/', (req, res) => {
  res.send(exampleController.exampleIndex());
});

router.get('/:id', (req, res) => {
  res.send(`example working with id ${req.params.id}`);
});

export default router;
